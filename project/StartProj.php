<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 11:56
 */
namespace project;

use Classes\Abstr\CreatorAbstract;
use Classes\Creators\DocCreator;
use Classes\Creators\HtmlCreator;
use Classes\Creators\TxtCreator;
use Classes\Creators\XlsCreator;


class StartProj
{
    public function index(){
       // DocCreator
       // HtmlCreator
       // TxtCreator
       // XlsCreator
        return $this->clientRender(new DocCreator());
    }

    public function clientRender(CreatorAbstract $creator){
        return htmlspecialchars($creator->buildDoc('Проверочный текст'));
    }
}