<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 15:58
 */
namespace Classes\Creators;



use Classes\Abstr\CreatorAbstract;
use Classes\Builders\XlsBuilder;
use Classes\interfaces\BuilderInterface;

class XlsCreator extends CreatorAbstract
{

    public function factoryMethod(): BuilderInterface{
        return new XlsBuilder();
    }

}