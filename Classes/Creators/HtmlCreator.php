<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 15:58
 */
namespace Classes\Creators;



use Classes\Abstr\CreatorAbstract;
use Classes\Builders\HtmlBuilder;
use Classes\interfaces\BuilderInterface;

class HtmlCreator extends CreatorAbstract
{

    public function factoryMethod(): BuilderInterface{
        return new HtmlBuilder();
    }

}