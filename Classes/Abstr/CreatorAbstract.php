<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 15:58
 */

namespace Classes\Abstr;

use Classes\interfaces\BuilderInterface;

abstract class CreatorAbstract
{
    public abstract function factoryMethod(): BuilderInterface;


    public function buildDoc($text): string
    {
        $builder = $this->factoryMethod();
        $builder->produceHeader();
        $builder->produceBlock($text);
        $builder->produceEnding();

        return    $builder->getProduct()->getResult();
    }

}