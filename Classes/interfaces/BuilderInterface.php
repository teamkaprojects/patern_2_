<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 11:53
 */
namespace Classes\interfaces;

use Classes\Products\Product;
interface BuilderInterface
{
    public function produceHeader();

    public function produceBlock($data);

    public function produceEnding();

    public function getProduct():Product;
}