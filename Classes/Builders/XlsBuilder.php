<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 15:58
 */
namespace Classes\Builders;

use Classes\interfaces\BuilderInterface;
use Classes\Products\Product;

class XlsBuilder implements BuilderInterface
{

    /*
     *
     <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <url><loc>' .  Di::route()->getMainUrl()   . $values['url'] . '</loc></url>

    </urlset>
     * */

    private $product;

    public function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $this->product = new Product();
    }


    public function produceHeader()
    {
        $this->product->header = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    }

    public function produceBlock($text)
    {
        $this->product->block = ' <url><loc>' . $text. '</loc></url>';
    }

    public function produceEnding()
    {
        $this->product->ending = '</urlset>';
    }

    public function getProduct(): Product
    {
        $result = $this->product;
        $this->reset();

        return $result;
    }

}