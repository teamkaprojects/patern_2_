<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 15:58
 */
namespace Classes\Builders;

use Classes\interfaces\BuilderInterface;
use Classes\Products\Product;

class TxtBuilder implements BuilderInterface
{

    private $product;

    public function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $this->product = new Product();
    }


    public function produceHeader()
    {
        $this->product->header = '';
    }

    public function produceBlock($text)
    {
        $this->product->block = $text;
    }

    public function produceEnding()
    {
        $this->product->ending = '';
    }

    public function getProduct(): Product
    {
        $result = $this->product;
        $this->reset();

        return $result;
    }

}